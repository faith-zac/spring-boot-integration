[本文参考博客](https://blog.csdn.net/qq_44769359/article/details/103634349?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161536701916780357214859%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=161536701916780357214859&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-103634349.pc_search_result_no_baidu_js&utm_term=springcloud%E5%88%9D%E5%AD%A6)

[Feign问题的博客](https://blog.csdn.net/Mr_FLM/article/details/94854319?ops_request_misc=&request_id=&biz_id=102&utm_term=springcloud中feign&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-5-94854319.pc_search_result_no_baidu_js)

- Eureka 注册中心
  - RESTTemplate
- Eureka 负载均衡
- Netflix 断路器
  - 合并请求
- Feign 声明式事务
- Zuul 微服务网关
- Config(并没有测试成功可以,换为本地)

|              | **Dubbo** | ****Spring Cloud**** |
| ------------ | :-------: | --------- |
| 服务注册中心 | Zookeeper | Spring Cloud Netflix Eureka |
| 服务调用方式 | RPC | REST API |
| 服务监控     | Dubbo-monitor | Spring Boot Admin |
| 断路器       | 不完善 | Spring Cloud Netflix Hystrix |
| 服务网关     | 无 | Spring Cloud Netflix Zuul |
| 分布式配置   | 无 | Spring Cloud Config |
| 服务跟踪     | 无 | Spring Cloud Sleuth |
| 消息总线     | 无 | Spring Cloud Bus |
| 数据流       | 无 | Spring Cloud Stream |
|批量任务		| 无 | Spring Cloud Task |

package cn.atfaith;/*
 * @author zac
 * @creat 2021-03-10 11:57
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationB2 {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationB2.class);
    }
}

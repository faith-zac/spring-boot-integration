package cn.atfaith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Admin
 *  2021-03-10 10:17
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServeApp {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServeApp.class, args);
    }
}

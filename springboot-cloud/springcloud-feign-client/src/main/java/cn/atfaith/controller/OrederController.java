package cn.atfaith.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Admin
 * @creat 2021-03-10 14:06
 */

@RestController
public class OrederController {

    @RequestMapping("/order")
    public Map<String,Object> order(){
        Map<String,Object> order=new HashMap<>();
        order.put("orderId",System.currentTimeMillis());
        order.put("merchantId",System.currentTimeMillis());
        order.put("note","强破破");
        return order;

    }
}


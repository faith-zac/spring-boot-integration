package cn.atfaith.controller;

import cn.atfaith.feign.OrderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Admin
 * @creat 2021-03-10 14:15
 */

@RestController
public class MerchantController {

    @Autowired
    OrderFeignClient orderFeignClient;

    @RequestMapping("/Merchant/order")
    public Map<String,Object> order(){
        Map<String ,Object> result= orderFeignClient.order();
        result.put("note2","我来自于 Feign server");
        return result;
    }

}


package cn.atfaith.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

/**
 * 这个接口会被注入springIOC容器成为-Bean
 * 这和我们的Mybatis 里面的@Mapper 对应的接口是非常类似的
 * @author Admin
 */
@FeignClient("feign-client")//这里是客户端的名称
public interface OrderFeignClient {

    @PostMapping("/order")
    Map<String,Object> order();
}

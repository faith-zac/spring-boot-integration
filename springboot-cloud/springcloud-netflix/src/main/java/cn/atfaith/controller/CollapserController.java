package cn.atfaith.controller;

import cn.atfaith.service.CollapseService;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Future;

/**
 * @author Admin
 * @creat 2021-03-10 13:54
 */

@RestController
public class CollapserController {

    @Autowired
    CollapseService collapseService;

    @GetMapping("/list")
    public List<Integer> getStroeList()throws Exception{
        HystrixRequestContext.initializeContext();
        Future<Integer> store1=collapseService.getStore(new Random().nextInt());
        Thread.sleep(100L);
        Future<Integer> store2 =collapseService.getStore(new Random().nextInt());
        List<Integer> result =new ArrayList<>();
        result.add(store1.get());
        result.add(store2.get());
        return result;
    }

}


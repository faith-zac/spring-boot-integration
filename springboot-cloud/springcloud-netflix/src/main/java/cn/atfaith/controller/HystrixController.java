package cn.atfaith.controller;/*
 * @author zac
 * @creat 2021-03-10 13:48
 */

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HystrixController {

    @GetMapping("/hello")
    @HystrixCommand(fallbackMethod = "hystrixHello")//这个注解是当这个方法发生异常
    //执行“hystrixHello”的
    public String hello(){
        //为了方便，我们直接抛出异常
        //throw new RuntimeException("xxx");
        //在我们正常执行时，执行此代码
        return "hello world";
    }

    public String hystrixHello(){
        return "hystrix Hello world";
    }

}


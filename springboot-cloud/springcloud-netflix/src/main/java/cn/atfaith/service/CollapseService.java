package cn.atfaith.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Future;

/**
 * @author Admin
 * @creat 2021-03-10 13:54
 */

@Service
public class CollapseService {

    Logger logger = LoggerFactory.getLogger(CollapseService.class);

    /**
     * timerDelayInMilliseconds时间延迟
     */
    @HystrixCollapser(batchMethod = "getStoreList", collapserProperties = {@HystrixProperty(name = "timerDelayInMilliseconds", value = "1000")})
    public Future<Integer> getStore(Integer id){
        return null;
    }

    @HystrixCommand
    public List<Integer> getStoreList(List<Integer> id){
        logger.info("合并请求调用 入参集合数量" +id.size());
        List<Integer> result =new ArrayList<>();
        for (Integer i:id){
            Integer rand =new Random().nextInt();
            logger.info("合并请求调用 输出"+rand);
            result.add(rand);
        }
        return result;
    }
}


package cn.atfaith.springbootmybatisplus.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zac
 * @since 2021-03-09
 */
@Controller
@RequestMapping("/mp-user")
public class MpUserController {

}

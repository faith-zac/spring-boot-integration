package cn.atfaith.springbootmybatisplus.mapper;

import cn.atfaith.springbootmybatisplus.domain.entity.MpUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zac
 * @since 2021-03-09
 */
public interface MpUserMapper extends BaseMapper<MpUser> {

}

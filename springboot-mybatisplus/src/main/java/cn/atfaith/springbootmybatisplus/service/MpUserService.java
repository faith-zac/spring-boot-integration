package cn.atfaith.springbootmybatisplus.service;

import cn.atfaith.springbootmybatisplus.domain.entity.MpUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zac
 * @since 2021-03-09
 */
public interface MpUserService extends IService<MpUser> {

}

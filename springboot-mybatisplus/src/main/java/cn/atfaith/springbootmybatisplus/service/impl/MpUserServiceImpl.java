package cn.atfaith.springbootmybatisplus.service.impl;

import cn.atfaith.springbootmybatisplus.domain.entity.MpUser;
import cn.atfaith.springbootmybatisplus.mapper.MpUserMapper;
import cn.atfaith.springbootmybatisplus.service.MpUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zac
 * @since 2021-03-09
 */
@Service
public class MpUserServiceImpl extends ServiceImpl<MpUserMapper, MpUser> implements MpUserService {

}

package cn.atfaith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 10:27
 */
@SpringBootApplication
public class WebSocket01 {
    public static void main(String[] args) {
        SpringApplication.run(WebSocket01.class);
    }
}

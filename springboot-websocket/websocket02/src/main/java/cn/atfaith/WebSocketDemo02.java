package cn.atfaith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 11:15
 */
@SpringBootApplication
public class WebSocketDemo02 {
    public static void main(String[] args) {
        SpringApplication.run(WebSocketDemo02.class);
    }
}

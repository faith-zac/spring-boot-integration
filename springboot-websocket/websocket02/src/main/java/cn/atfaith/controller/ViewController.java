package cn.atfaith.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 13:51
 */
@RequestMapping("/view")
@Controller
public class ViewController {

    @RequestMapping("/{name}")
    public String skip(@PathVariable(value = "name")String name) {
        return name;
    }
}

package cn.atfaith.pojo;

import lombok.Data;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 12:00
 */
@Data
public class SocketMsg {
    /**
     * 单发/群发
     */
    private int type;
    /**
     *发送
     */
    private String fromUser;
    /**
     *接收
     */
    private String toUser;
    /**
     *信息
     */
    private String msg;
}

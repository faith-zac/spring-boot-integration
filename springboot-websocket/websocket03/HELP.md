SpringBoot+STOMP

STOMP是WebSocket的子协议，STOMP即Simple (or Streaming) Text Orientated Messaging Protocol，简单(流)文本定向消息协议，它提供了一个可互操作的连接格式，允许STOMP客户端与任意STOMP消息代理（Broker）进行交互。STOMP协议由于设计简单，易于开发客户端，因此在多种语言和多种平台上得到广泛地应用。

[博客](https://blog.csdn.net/qq_41603102/article/details/88351729)

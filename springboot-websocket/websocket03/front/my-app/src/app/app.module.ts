import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {RouterModule, Routes} from "@angular/router";
import { WebsocketComponent } from './websocket/websocket.component';
import {ButtonModule} from 'primeng/button';
import {FormsModule} from "@angular/forms";

const routes:Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'websocket', component: WebsocketComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WebsocketComponent
  ],
  imports: [
    BrowserModule,
    ButtonModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class AppRoutingModule  { }

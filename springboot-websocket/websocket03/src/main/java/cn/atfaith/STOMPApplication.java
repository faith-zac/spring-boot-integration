package cn.atfaith;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 16:57
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class STOMPApplication {
    public static void main(String[] args) {
        SpringApplication.run(STOMPApplication.class);
    }
}

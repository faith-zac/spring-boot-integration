package cn.atfaith.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 17:11
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestMessage {
    /**
     * 消息发送者
     */
    private String sender;
    /**
     * 房间号
     */
    private String room;
    /**
     * 消息类型
     */
    private String type;
    /**
     * 消息内容
     */
    private String content;
}

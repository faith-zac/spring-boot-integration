package cn.atfaith.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZHAOAC
 * @creat 2021-03-12 17:11
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessage {
    /**
     * 发送者
     */
    private String sender;
    /**
     * 类型
     */
    private String type;
    /**
     * 内容
     */
    private String content;
}

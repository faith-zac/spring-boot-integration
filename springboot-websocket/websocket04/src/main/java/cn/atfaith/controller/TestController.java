package cn.atfaith.controller;

import cn.atfaith.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZHAOAC
 * @date 2021/3/16 17:04
 */
@RestController
public class TestController {

    @Autowired
    CommonService commonService;


    @GetMapping("/test/testAsync")
    public String testAsync() {
        commonService.testAsync();
        return "http请求已结束";
    }

}

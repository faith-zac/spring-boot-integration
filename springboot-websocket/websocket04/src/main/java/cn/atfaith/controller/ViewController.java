package cn.atfaith.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ZHAOAC
 * @date 2021/3/16 16:48
 */
@RequestMapping("/view")
@Controller
public class ViewController {
    @RequestMapping("/{viewName}")
    public String forward(@PathVariable("viewName") String viewName) {
        return viewName;
    }
}

package cn.atfaith.controller;

import cn.atfaith.pojo.RequestMessage;
import cn.atfaith.pojo.mq.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

/**
 * @author ZHAOAC
 * @date 2021/3/16 16:22
 */
@RestController
public class WebSocketTestController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    Sender senderMQ;

    /**聊天室（单聊+多聊）&&消息转发
     * @param requestMessage
     * @throws Exception
     */
    @CrossOrigin
    @MessageMapping("/chat")
    public void messageHandling(RequestMessage requestMessage) throws Exception {
        String destination = "/topic/" + HtmlUtils.htmlEscape(requestMessage.getRoom());

        //htmlEscape  转换为HTML转义字符表示
        String room = HtmlUtils.htmlEscape(requestMessage.getRoom());
        String type = HtmlUtils.htmlEscape(requestMessage.getType());
        String content = HtmlUtils.htmlEscape(requestMessage.getContent());
        String userId = HtmlUtils.htmlEscape(requestMessage.getUserId());
        String questionId = HtmlUtils.htmlEscape(requestMessage.getQuestionId());
        String createTime = HtmlUtils.htmlEscape(requestMessage.getCreateTime());

        System.out.println( requestMessage.getRoom() );
        System.out.println( content );




        messagingTemplate.convertAndSend(destination, requestMessage);
    }

}

package cn.atfaith.pojo;

/**
 * @author ZHAOAC
 * @date 2021/3/16 16:19
 */
public class RequestMessage {

    /**
     * 频道号
     */
    private String room;
    /**
     * 消息类型('1':客户端到服务端   '2'：客户端到服务端)
     */
    private String type;
    /**
     * 消息内容（即答案）
     */
    private String content;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 题目id
     */
    private String questionId;
    /**
     * 时间
     */
    private String createTime;

    public RequestMessage() {
    }

    public RequestMessage(String room, String type, String content, String userId, String questionId, String createTime) {
        this.room = room;
        this.type = type;
        this.content = content;
        this.userId = userId;
        this.questionId = questionId;
        this.createTime = createTime;
    }

    public String getRoom() {
        return room;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public void setType(String type) {
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

package cn.atfaith.service;

import cn.atfaith.pojo.RequestMessage;
import cn.atfaith.pojo.mq.Sender;
import cn.atfaith.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author ZHAOAC
 * @date 2021/3/16 17:04
 */
@Service
public class CommonServiceImpl implements CommonService {


    @Autowired
    Sender sender;


    @Async
    @Override
    public void testAsync() {

        RequestMessage mqTask = new RequestMessage(  );
        for(int i=0;i<6;i++) {
            mqTask.setRoom( "123");
            mqTask.setUserId("000");
            mqTask.setType( "2" );
            mqTask.setQuestionId( "0000");
            mqTask.setCreateTime( "0000");
            mqTask.setContent("this:"+i);

            sender.send( JsonUtils.objectToJson( mqTask ) );
            try {
                Thread.sleep( 1000 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }





}
